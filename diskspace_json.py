import json
import psutil

def round_bytes(num):
    for unit in ['B','KB','MB','GB','TB','PB','EB','ZB']:
        if abs(num) < 1024.0:
            return "%3.1f %s" % (num, unit)
        num /= 1024.0
    return "%.1f %s" % (num, 'YB')

hdd = psutil.disk_usage('/')

output = {}
for name in ['total', 'free', 'used']:
    output[name] = {}
    output[name]["bytes"] = getattr(hdd, name)
    output[name]["human"] = round_bytes(getattr(hdd, name))
    output[name]["percent"] = round(getattr(hdd, name)/hdd.total*100, 2)

print (json.dumps(output))