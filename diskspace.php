<?php //https://www.php.net/manual/en/function.disk-total-space.php#75971
function getSymbolByQuantity($bytes) {
	$symbols = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$exp = floor(log($bytes)/log(1024));

	return sprintf('%.2f '.$symbols[$exp], ($bytes/pow(1024, floor($exp))));
}

$df = disk_free_space("/");
$ds = disk_total_space("/");

$percent = round(100-($df/$ds*100), 2);
			
$du = getSymbolByQuantity($ds-$df);
$df = getSymbolByQuantity($df);
$ds = getSymbolByQuantity($ds);
?>
Total disk space: <?= $ds ?><br />
Disk space left: <?= $df ?><br />
Disk space used: <?= $du ?><br />
Disk space left percentually: <?= (100-$percent) ?>%<br /><br />

<div style="height:50px;width:200px;background-color:green;">
	<div style="height:100%;width:<?= round($percent*2) ?>px;background-color:red;float:left;">
</div>