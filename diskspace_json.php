<?php //https://www.php.net/manual/en/function.disk-total-space.php#75971
header('Content-Type: application/json');

function getSymbolByQuantity($bytes) {
	$symbols = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$exp = floor(log($bytes)/log(1024));

	return sprintf('%.2f '.$symbols[$exp], ($bytes/pow(1024, floor($exp))));
}

$df = disk_free_space("/");
$ds = disk_total_space("/");
$du = $ds-$df;

$output = [];
foreach(["total" => $ds, "free" => $df, "used" => $du] as $key => $val) {
	$output[$key] = [];
	$output[$key]["bytes"] = $val;
	$output[$key]["human"] = getSymbolByQuantity($val);
	$output[$key]["percent"] = round($val/$ds*100, 2);
}

echo json_encode($output);